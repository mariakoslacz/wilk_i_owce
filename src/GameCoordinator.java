import java.util.Map;

public class GameCoordinator {

    public static void runGame() {
        Wolf wolf = new Wolf();
        Sheep sheep = new Sheep();
        boolean hasEnded = false;
        int boardSize = User.chooseBoardSize();
        int turn = 0;

        sheep.placeSheep(boardSize);
        wolf.setPosition(User.chooseWolfPosition(boardSize));

        while (!hasEnded) {
            GraphicalBoard.generateGraphics(boardSize, wolf.getPosition(), sheep.getPositions());
            hasEnded = nextTurn(turn, boardSize, sheep, wolf);
            turn++;
        }

        User.closeScanner();
        printEndMessage(turn);
    }

    private static boolean nextTurn(int turnNumber, int boardSize, Sheep sheep, Wolf wolf) {
        if ((turnNumber % 2) == 0) {
            return wolfTurn(boardSize, wolf, sheep.getPositions());
        } else {
            return sheepTurn(boardSize, sheep, wolf);
        }
    }

    private static boolean wolfTurn(int boardSize,
                                    Wolf wolf,
                                    Map<String, int[]> sheepPositions) {
        int[] move;

        Wolf.printInstruction();

        do {
            move = User.readMove();
        } while (!wolf.isValidMove(move, boardSize, sheepPositions));

        wolf.setPosition(wolf.calculatePosition(move));

        return wolf.hasWon();
    }

    private static boolean sheepTurn(int boardSize,
                                     Sheep sheep,
                                     Wolf wolf) {
        int[] move;

        String sheepName = User.chooseSheep();
        Sheep.printInstruction();

        do {
            move = User.readMove();
        } while (!sheep.isValidMove(move, boardSize, wolf.getPosition(), sheepName));

        sheep.setPosition(sheepName, sheep.calculatePosition(sheepName, move));

        return sheep.haveWon(wolf, boardSize);
    }

    private static void printEndMessage(int turn) {
        if (turn % 2 == 0)
            System.out.println("Sheep have won!");
        else
            System.out.println("Wolf has won!");
    }
}
