import java.util.Arrays;
import java.util.Map;

public class Wolf {

    private int[] position = new int[2];

    public static void printInstruction() {
        System.out.println("Wolf chooses his move: ");
        System.out.println("1: down-left");
        System.out.println("2: down-right");
        System.out.println("3: up-left");
        System.out.println("4: up-right");
    }

    public boolean isValidMove(int[] move,
                               int boardSize,
                               Map<String, int[]> sheepPositions) {
        int[] newPosition = calculatePosition(move);

        return isMoveCorrectOption(move)
                && isPositionWithinBounds(newPosition, boardSize)
                && isPositionNotTaken(newPosition, sheepPositions);
    }

    private boolean isMoveCorrectOption(int[] move) {
        return ((move[0] == -1) || (move[0] == 1))
                && ((move[1] == -1) || (move[1] == 1));
    }

    private boolean isPositionWithinBounds(int[] newPosition, int boardSize) {
        return (newPosition[0] >= 0 &&
                newPosition[0] < boardSize &&
                newPosition[1] >= 0 &&
                newPosition[1] < boardSize);
    }

    private boolean isPositionNotTaken(int[] newPosition,
                                              Map<String, int[]> positions) {
        for (Map.Entry<String, int[]> entry : positions.entrySet()) {
            if (Arrays.equals(entry.getValue(), newPosition))
                return false;
        }

        return true;
    }

    public int[] calculatePosition(int[] move) {
        return new int[] {
                position[0] + move[0], position[1] + move[1]
        };
    }

    public boolean hasWon() {
        return position[0] == 0;
    }

    public int[] getPosition() {
        return position;
    }

    public void setPosition(int[] newPosition) {
        position = newPosition;
    }
}
