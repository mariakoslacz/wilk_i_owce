import java.util.InputMismatchException;
import java.util.Scanner;

public class User {
    final private static Scanner SCANNER = new Scanner(System.in);
    private static final String[] SHEEP_COLOURS = {"\033[1;31m red",
                                                    "\033[1;32m green",
                                                    "\033[1;34m blue",
                                                    "\033[1;35m purple"};

    private static boolean validateBoardSize(int boardSize) {
        return (boardSize % 4 == 0) && (boardSize >= 8);
    }

    private static boolean validateWolfsPosition(int pos, int n) {
        return (pos % 2 == 0) && pos >= 0 && pos < n;
    }

    public static int chooseBoardSize() {
        boolean isCorrect;
        int size;

        System.out.println("Choose board size: ");
        do {
            size = SCANNER.nextInt();
            isCorrect = validateBoardSize(size);
            if (!isCorrect)
                System.out.println("Board size must be a product of 4 and not smaller than 8!");
        } while (!isCorrect);

        return size;
    }

    public static int[] chooseWolfPosition(int boardSize) {
        boolean isCorrect;
        int position;

        System.out.println("Choose wolf's position: ");
        do {
            position = SCANNER.nextInt();
            isCorrect = validateWolfsPosition(position, boardSize);
            if (!isCorrect)
                System.out.println("Wolf's position must be even!");
        } while (!isCorrect);

        return new int[]{boardSize - 1, position};
    }

    public static String chooseSheep() {
        int choice;
        boolean isCorrect = false;

        System.out.println("Choose sheep: ");
        for (int i = 0; i < 4; i++) {
            System.out.printf("%d: %s \u001B[0m \n", i + 1, SHEEP_COLOURS[i]);
        }

        do {
            choice = SCANNER.nextInt();
            if ((choice > 0) && (choice <= 4)) {
                isCorrect = true;
            }

            if (!isCorrect)
                System.out.println("Sheep number must be either 1, 2, 3 or 4!");
        } while (!isCorrect);

        System.out.printf("You've chosen%s sheep \u001B[0m \n", SHEEP_COLOURS[choice - 1]);

        return "s" + choice;
    }

    public static int[] readMove() {
        System.out.println("Choose your move: ");

        return switch (SCANNER.nextInt()) {
            case 1 -> new int[]{1, -1};
            case 2 -> new int[]{1, 1};
            case 3 -> new int[]{-1, -1};
            case 4 -> new int[]{-1, 1};
            default -> new int[]{0, 0};
        };
    }

    public static void closeScanner() {
        SCANNER.close();
    }
}
