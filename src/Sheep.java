import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Sheep {

    private final Map<String, int[]> positions = new HashMap<>(4);

    public void placeSheep(int size) {
        int initIndex = (size % 8 == 0) ? (size / 4) - 1 : (size / 4);
        positions.put("s1", new int[]{0, initIndex});
        positions.put("s2", new int[]{0, initIndex + ((size / 4) - ((size / 4 )% 2))});
        positions.put("s3", new int[]{0, initIndex + 2 * ((size / 4) - ((size / 4 )% 2))});
        positions.put("s4", new int[]{0, initIndex + 3 * ((size / 4) - ((size / 4 )% 2))});
    }

    public static void printInstruction() {
        System.out.println("Sheep choose their move: ");
        System.out.println("1: down-left");
        System.out.println("2: down-right");
    }

    public boolean isValidMove(int[] move,
                                      int boardSize,
                                      int[] wolfPosition,
                                      String sheepName) {
        int[] newPosition = calculatePosition(sheepName, move);

        return isMoveCorrectOption(move)
                && isPositionWithinBounds(newPosition, boardSize)
                && isPositionNotTaken(newPosition, wolfPosition, sheepName);
    }

    private boolean isMoveCorrectOption(int[] move) {
        return (move[0] == 1) && ((move[1] == -1) || (move[1] == 1));
    }

    private boolean isPositionWithinBounds(int[] newPosition, int size) {
        return (newPosition[0] >= 0 &&
                newPosition[0] < size &&
                newPosition[1] >= 0 &&
                newPosition[1] < size);
    }

    private boolean isPositionNotTaken(int[] newPosition,
                                              int[] wolfPosition,
                                              String sheepName) {
        for (Map.Entry<String, int[]> entry : positions.entrySet()) {
            if (!entry.getKey().equals(sheepName) &&
                    Arrays.equals(entry.getValue(), newPosition))
                return false;
        }

        return !Arrays.equals(wolfPosition, newPosition);
    }

    public int[] calculatePosition(String name, int[] move) {
        return new int[] {positions.get(name)[0] + move[0], positions.get(name)[1] + move[1]};
    }

    public boolean haveWon(Wolf wolf, int boardSize) {
        for(int i = -1; i <= 1; i += 2) {
            for (int n = -1; n <= 1; n += 2) {
                if (wolf.isValidMove(new int[] {i, n}, boardSize, positions))
                    return false;
            }
        }
        return true;
    }

    public Map<String, int[]> getPositions() {
        return positions;
    }

    public void setPosition(String name, int[] newPosition) {
        positions.replace(name, newPosition);
    }
}
