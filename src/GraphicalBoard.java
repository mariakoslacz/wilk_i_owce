import java.util.Map;

public class GraphicalBoard {

    private static final String WHITE_CHECK = "\u001B[47m";
    private static final String BLACK_CHECK = "\u001B[40m";
    private static final String WOLF_COLOUR = "\033[1;37m";
    private static final String SHEEP1_COLOUR = "\033[1;31m";
    private static final String SHEEP2_COLOUR = "\033[1;32m";
    private static final String SHEEP3_COLOUR = "\033[1;34m";
    private static final String SHEEP4_COLOUR = "\033[1;35m";
    private static final String DEFAULT_COLOUR = "\u001B[0m";

    public static void generateGraphics(int boardSize,
                                        int[] wolfPosition,
                                        Map<String, int[]> sheepPosition) {
        char[][] rows = setPositions(
                generateEmptyBoard(boardSize),
                wolfPosition,
                sheepPosition);

        for (char[] row: rows) {
            StringBuilder rowBuilder = new StringBuilder();

            for (char square: row) {
                if (square == '#') {
                    rowBuilder.append(WHITE_CHECK).append("   ");
                } else if (square == '1') {
                    rowBuilder.append(BLACK_CHECK).append(SHEEP1_COLOUR).append(" O ");
                } else if (square == '2') {
                    rowBuilder.append(BLACK_CHECK).append(SHEEP2_COLOUR).append(" O ");
                } else if (square == '3') {
                    rowBuilder.append(BLACK_CHECK).append(SHEEP3_COLOUR).append(" O ");
                } else if (square == '4') {
                    rowBuilder.append(BLACK_CHECK).append(SHEEP4_COLOUR).append(" O ");
                } else if (square == 'W') {
                    rowBuilder.append(BLACK_CHECK).append(WOLF_COLOUR).append(" @ ");
                } else {
                    rowBuilder.append(BLACK_CHECK).append("   ");
                }
            }

            System.out.println(rowBuilder.append(DEFAULT_COLOUR));
        }
    }

    public static char[][] generateEmptyBoard(int boardSize) {
        char[][] result = new char[boardSize][boardSize];
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if ((i + j) % 2 == 0)
                    result[i][j] = '#';
                else
                    result[i][j] = ' ';
            }
        }
        return result;
    }

    private static char[][] setPositions(char[][] board,
                                         int[] wolfPosition,
                                         Map<String, int[]> sheepPositions) {
        // Place sheep
        for (Map.Entry<String, int[]> position: sheepPositions.entrySet()) {
                board[position.getValue()[0]][position.getValue()[1]]
                        = position.getKey().charAt(1);
        }

        // Place wolf
        board[wolfPosition[0]][wolfPosition[1]] = 'W';
        return board;
    }
}
