import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;


class UserTest {

    @Test
    void testChooseSize() {
        ByteArrayInputStream in =
                new ByteArrayInputStream("8".getBytes(StandardCharsets.UTF_8));

        System.setIn(in);
        assertEquals(0, User.chooseBoardSize() % 4);
    }

    @Test
    void testWrongSize() {
        ByteArrayInputStream in =
                new ByteArrayInputStream("7 \n 8".getBytes(StandardCharsets.UTF_8));

        System.setIn(in);
        assertEquals(0, User.chooseBoardSize() % 4);
    }

    @Test
    void testChooseWolfsPosition() {
        int boardSize = 8;
        int[] expected = new int[] {7, 2};
        ByteArrayInputStream in =
                new ByteArrayInputStream("-1 \n 7 \n 2".getBytes(StandardCharsets.UTF_8));

        System.setIn(in);
        assertArrayEquals(expected, User.chooseWolfPosition(boardSize));
    }

    @Test
    void testChooseSheep() {
        ByteArrayInputStream in = new ByteArrayInputStream("-1 \n 5 \n 3".getBytes(StandardCharsets.UTF_8));
        System.setIn(in);
        assertEquals("s3", User.chooseSheep());
    }

    @Test
    void testReadMoveForWolf() {

    }
}