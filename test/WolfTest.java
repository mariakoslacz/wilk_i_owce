import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class WolfTest {

    @Test
    void testIsValidMove() {
        final Map<String, int[]> positions = new HashMap<>(4);
        positions.put("s1", new int[]{0, 1});
        positions.put("s2", new int[]{0, 3});
        positions.put("s3", new int[]{0, 5});
        positions.put("s4", new int[]{0, 7});
        final Wolf wolf = new Wolf();
        wolf.setPosition(new int[] {1, 0});

        assertAll(
                () -> assertTrue(wolf.isValidMove(new int[] {1, 1}, 8, positions)),
                () -> assertFalse(wolf.isValidMove(new int[] {1, -1}, 8, positions)),
                () -> assertFalse(wolf.isValidMove(new int[] {1, 2}, 8, positions)),
                () -> assertFalse(wolf.isValidMove(new int[] {-1, 1}, 8, positions))
        );
    }
}